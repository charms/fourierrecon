function [fHat, fxy ] = Get2DFourierCoefficients(FncType, N, M)
%% Generate Fourier Series Coefficients of 2D Test Function
% Script to generate the Fourier coefficients of a 2D test function 
%
% Usage:    [fHat, fxy] = GetFourierCoefficients(FncType, N, M)
%
%   Inputs:
%       FncType     - the type of test function to generate (string)
%                     
%       N           - Number of Fourier coefficients to compute
%                     in the k_x direction  (integer)
%
%       M           - Number of Fourier coefficients to compute 
%                     in the k_y direction  (integer)
%
%   Outputs:
%       fHat        - the Fourier coefficients (complex matrix)
%
%       fxy         - function handle to construct (physical-space)
%                     function on a grid (usage: fxy(z), where z is a
%                     matrix of grid points)
%


%% Initialization
FncType = lower(FncType);

% Fourier coefficients to compute
fHat = zeros(2*N+1,2*M+1);

% Compute Fourier coefficients
% Note: We will assume that the function is 2pi periodic
switch FncType

    % 2D box function
    %            |  1,    |x|, |y| <= 1
    %  f(x,y) =  |
    %            |  0,      else
    case ('box')
        
        kN = -N:N;
        kM = -M:M;
        
        % Fourier coefficients
        [kx, lx] = meshgrid(kN, kM);
        fHat = (1/(pi^2)) .* sin(kx).*sin(lx) ./ (kx.*lx);
    
        % when k=0 or l=0
        fHat(kx==0) = sin(lx(kx==0)) ./ (pi^2 .* lx(kx==0));
        fHat(lx==0) = sin(kx(lx==0)) ./ (pi^2  .*kx(lx==0));
        % when k=l=0
        fHat(M+1, N+1) = 1 / (pi^2);
               
        % Physical space function
        fxy = @(x,y)    0 + ...
                        1 * ( (x >= -1) & (x <= 1) & (y >= -1 ) & (y <= 1));
    
                    
    % 2D circle function
    %           |    1,            sqrt(x^2+y^2) <= 1
    %  f(x,y) = |
    %           |    0,                    else
    case ('circle')
        kN = -N:N;
        kM = -M:M;
       
        [kx, lx] = meshgrid(kN, kM);
        rx = sqrt(kx.^2 + lx.^2);
        
        % Fourier coefficients
        fHat = besselj(1, rx) ./ (2*pi * rx); 
        fHat(rx == 0) = 1/(4*pi);
        
        % Physical space function
        fxy = @(x,y)    0 + ...
                        1 * (sqrt(x.^2 + y.^2) <= 1);

                    
    % Shifted, scaled box and circle functions
    % f(x,y) = (1/2) *f_cir(x-0.50, y-1.00) + 
    %          (3/4) *f_box(x+1.25, y+1.50) + ...
    %          (0.35)*f_circ(2*(x-1.25), 2*(y+1.25));

    case ('circle-square')
        kN = -N:N;
        kM = -M:M;
       
        [kx, lx] = meshgrid(kN, kM);  
        rx = sqrt(kx.^2 + lx.^2);
        
        % Fourier coefficients of the (standard) circle function
        fHat_circ = besselj(1, rx) ./ (2*pi * rx); 
        fHat_circ(rx == 0) = 1/(4*pi);
        
        % Physical space circle function
        fxy_circ = @(x,y)    0 + ...
                        1 * (sqrt(x.^2 + y.^2) <= 1);

        % Fourier coefficients of the (standard) box function        
        fHat_sq = (1/(pi^2)) .* sin(kx).*sin(lx) ./ (kx.*lx);
        
        fHat_sq(kx==0) = sin(lx(kx==0)) ./ (pi^2 .* lx(kx==0));
        fHat_sq(lx==0) = sin(kx(lx==0)) ./ (pi^2  .*kx(lx==0));
        fHat_sq(M+1, N+1) = 1 / (pi^2);
               
        % Physical space box function
        fxy_sq = @(x,y)    0 + ...
                        1 * ( (x >= -1) & (x <= 1) & (y >= -1 ) & (y <= 1));
                    
                    
        % Fourier coefficients of shifted/scaled circle function
        rx = sqrt((kx/2).^2 + (lx/2).^2);
        fHat_circ_scaled = besselj(1, rx) ./ (2*pi * rx); 
        fHat_circ_scaled(rx == 0) = 1/(4*pi);
        fHat_circ_scaled = fHat_circ_scaled/4;
                    
          
        % Composite box-circle function in physical space
        fxy = @(x,y) 0.50*fxy_circ(x-0.5, y-1) + 0.75*fxy_sq(x+1.25, y+1.5) + ...
                     0.35*fxy_circ(2*(x-1.25), 2*(y+1.25));
                 
        % ... and the corresponding Fourier coefficients         
        fHat =  0.50*fHat_circ        .* exp(-1i*(kx*0.5+lx*1)) + ...
                0.75*fHat_sq          .* exp(-1i*(-kx*1.25-lx*1.5)) + ...
                0.35*fHat_circ_scaled .* exp(-1i*(kx*1.25-lx*1.25));
                    

end

return


