% Reconstruction from Fourier spectral data
% This script demonstrates presents preliminary 2D results concerning 
% edge-augmented Fourier partial sums

clear; close all; clc
addpath ../../src

% for pretty pictures
set(0,'defaulttextinterpreter','latex');
set(0,'defaultLineLineWidth',2);
set(0,'DefaultAxesFontSize',14);
set(0,'DefaultLegendInterpreter','latex')


%% Initialization and parameters

% Get (analytically computed) 2D Fourier coefficients for test function
% We use a test function made up of shifted squares and circles
M = 025; N = 025;
[fHat, fxy] = Get2DFourierCoefficients('circle-square', N, M);


%% Standard Fourier reconstruction
% Fourier modes
kn = (-N:N).';
km = (-M:M).';

ngrid = 3*(2*N+1)+1;      % reconstruct on 3x oversampled grid
mgrid = 3*(2*M+1)+1;

% Generate equispaced grid in [-pi, pi)^2
x = -pi + (2*pi/ngrid)*( 0:ngrid-1 ).';
y = -pi + (2*pi/mgrid)*( 0:mgrid-1 ).';

% 2D Partial Fourier sum
% Note/Todo: use FFTs for added efficiency
F1 = exp(1i * x * kn.');
F2 = exp(1i * y * km.');

% Fourier partial sum
S_NMf = F1 * fHat * F2.';

% 2D reconstruction grid
[xx,yy] = meshgrid(x,y);


%% Proposed Edge-Augmented Method

% Suppress jump height warnings
warning( 'off', 'signal:findpeaks:largeMinPeakHeight' );

% Fine grid along the y direction
n_fine = N^2;
y_fine = -pi + (2*pi/n_fine)*( 0:n_fine-1 ).';

fprintf( '\n Now computing edge-augmented reconstruction...');
fprintf( '\n Now evaluating row cross-sections...' );

% Reconstruct along each row...
row_recon = zeros(length(y_fine), length(x));
for ix = 1:length(y_fine)
    % Fourier Coefficients for cross section at y=y_j
    cfs = fHat.'*exp(1i*km*y_fine(ix));
    % Estimate jumps 
    [jmp_locs, jmp_ht]   = FindJumps(cfs, 'conc', true, [], [], 0.1); 
    % Edge-augmented reconstruction
    row_recon(ix,:) = EdgeEnhancedReconstruction(...
                                    cfs, jmp_ht, jmp_locs, ngrid).';
end

fprintf( '\n Now refining column cross-sections...' );
% Now refine along each column...
col_recon = zeros(length(y), length(x));
for ix = 1:length(x)
    % Fourier Coefficients for cross section at x=x_k
    % Compute these using quadrature
    cfs = exp(-1i*kn*y_fine.')*row_recon(:,ix)/n_fine;
    % Estimate jumps 
    [jmp_locs, jmp_ht]   = FindJumps(cfs, 'conc', true, [], [], 0.1); 
    % Edge-augmented reconstruction
    col_recon(:,ix) = EdgeEnhancedReconstruction(...
                                    cfs, jmp_ht, jmp_locs, ngrid);
end


%% Display results
figure; imagesc(x, y, real(S_NMf)); title 'Fourier Partial Sum'; colorbar
set(gca, 'YDir', 'normal')
true_f = fxy(xx, yy);

figure; imagesc(x, y, col_recon); title 'Edge-Augmented Fourier Sum'; 
colorbar; set(gca, 'YDir', 'normal')

fprintf( '\n\n PSNR for (standard) Fourier partial sum is %3.3f', ...
                                    psnr(S_NMf, true_f) );
fprintf( '\n PSNR for edge-augmented Fourier partial sum is %3.3f\n\n', ...
                                    psnr(col_recon, true_f) );
                                