% Reconstruction from Fourier spectral data
% This script demonstrates edge-augmented Fourier partial sums and compares
% their accuracy with compressive sensing based reconstructions


clear; close all; clc
addpath ../../src
addpath ~/software/cvx/             % Add path to CVX here


% for pretty pictures
set(0,'defaulttextinterpreter','latex');
set(0,'defaultLineLineWidth',2);
set(0,'DefaultAxesFontSize',14);
set(0,'DefaultLegendInterpreter','latex')

% Set random seed
rng(1234);


% Initialization and Parameters
N = 050;                        % No. of Fourier coefficients is 2N+1

% Get (analytically computed) Fourier coefficients
[fHat, f, jmp_f] = GetFourierCoefficients( 'multi-box', N );

% Fourier partial sum reconstruction
[S_Nf, x] = ComputeFourierReconstruction(fHat);

% Edge-enhanced sum (with true edges)
S_Nf_edge = EdgeEnhancedReconstruction(fHat, jmp_f(:,1), jmp_f(:,2));

% Edge-enhanced sum with estimated edges
[jmp_locs, jmp_ht]   = FindJumps(fHat, 'conc', true); 
S_Nf_edge_est = EdgeEnhancedReconstruction(fHat, jmp_ht, jmp_locs);


%% Compressed Sensing Reconstruction
% Grid size
ngrid = length(x);

% Derivative matrix (for TV norms)
h = x(2)-x(1);
e = ones(ngrid, 1);
D = spdiags([-e e], 0:1, ngrid, ngrid)/h;

% Reg. parameter
lambda = 1e-4;

% randomly choose Fourier frequencies
M = 3*N;
[fk, ~] = GetFourierCoefficients( 'multi-box',  M);
smpld_freq = randperm(2*M+1, 2*N+1).';
k = (-M:M).';

% DFT matrix
DFTMat = exp( -1i*k*x.' )/ngrid;

% CS problem formulation (TV minimization, implemented using CVX)
% Suppress CVX warnings about soon-to-be deprecated features
warning( 'off', 'MATLAB:nargchk:deprecated' );

cvx_begin
    cvx_precision high
    cvx_quiet true
    variable CSRecon(ngrid,1);
    
    minimize norm(DFTMat(smpld_freq,:)*CSRecon - fk(smpld_freq)) + ...
                                        lambda*norm(D*CSRecon, 1);
cvx_end


%% Plot
plot(x, f(x), '--'); hold on; plot(x, S_Nf, ':')
plot(x, S_Nf_edge_est, '-.'); plot(x, CSRecon);
grid; xlim([-pi pi])
xlabel '$x$'; ylabel 'Reconstruction'
legend( '$f_1$ (True Function)', ...
        '$S_N f_1$ (Fourier Partial Sum)', ...
        '$\widetilde f_1$ (Edge-Augmented Reconstruction)', ...
        'CS Reconstruction', ...
        'location', 'southwest' );
    
    
% Plot error
figure; semilogy(x, abs(f(x)-S_Nf), '--' ); hold on
semilogy(x, abs(f(x)-S_Nf_edge_est), ':' ); semilogy(x, abs(f(x)-CSRecon)); 
xlim([-pi pi]); xlabel '$x$'; ylabel 'Absolute Error'; grid
legend( '$|f_2-S_Nf_2|$ (Error in Partial Fourier Sum)', ...
        '$|f_2-\widetilde f_2|$ (Error in Edge-Augmented Recon.)', ...
        '$|f_2-f_2^{CS}|$ (Error in CS Recon.)', ...
        'location', 'southwest' );