% Convergence of Fourier approximation

clear; close all; clc
addpath ../../src

% for pretty pictures
set(0,'defaulttextinterpreter','latex');
set(0,'defaultLineLineWidth',2);
set(0,'DefaultAxesFontSize',14);
set(0,'DefaultLegendInterpreter','latex')

% Set random seed
rng(12345);


%% Parameters
snrvals = (100:-10:10).';                 % Values of SNR
N = 100;                                  % No. of Fourier measurements
err_2           = zeros( size(snrvals) ); % Store error (2-norm) here
err_edge_2      = zeros( size(snrvals) );
err_edge_est_2  = zeros( size(snrvals) );

nTrials = 100;                             % No. of trials for plot


%% Initialization
% Get Fourier coefficients
[fHat, f, jmp_f] = GetFourierCoefficients( 'piecewise', N );

% Noiseless reconstruction
% Fourier sum
[S_Nf, x] = ComputeFourierReconstruction(fHat);
% Edge-enhanced sum (with true edges)
S_Nf_edge = EdgeEnhancedReconstruction(fHat, jmp_f(:,1), jmp_f(:,2));
% Edge0enhanced sum with estimated edges
[jmp_locs, jmp_ht]   = FindJumps(fHat, 'conc', true); 
S_Nf_edge_est = EdgeEnhancedReconstruction(fHat, jmp_ht, jmp_locs);
    
% Compute reconstruction error for noiseless reconstruction
err_nonoise_SNf         = 10*log10( norm(f(x)-S_Nf)^2 / norm(f(x))^2 );
err_nonoise_SNfedge     = 10*log10( norm(f(x)-S_Nf_edge)^2 / norm(f(x))^2 );
err_nonoise_SNfedge_est = 10*log10( norm(f(x)-S_Nf_edge_est)^2 / norm(f(x))^2 );

    
%% Convergence loop
for ix = 1:length(snrvals)
        
    fprintf( '\n Now evaluating noise robustness at SNR %ddB ...', snrvals(ix) );
    
    for itrials = 1:nTrials
        % Add noise
        snr = snrvals(ix);                              % SNR
        sig_power = norm(fHat)^2/length(fHat);          % Signal power
        noise_power = sig_power / ( 10^(snr/10) );      % Noise power
        sig2 = noise_power;                             % Eqv. noise variance
        noise = sqrt(sig2/2)*( randn(2*N+1, 1) + 1i*randn(2*N+1, 1) );
        gHat = fHat + noise;    

        % Fourier partial sum
        [S_Nf, x] = ComputeFourierReconstruction(gHat);
        % Edge-Enhanced Fourier partial sum (with true edges)
        S_Nf_edge = EdgeEnhancedReconstruction(gHat, jmp_f(:,1), jmp_f(:,2));

        % (estimated edge information) Edge-Enhanced Fourier partial sum
        % Jump detection - conc. method
        [jmp_locs, jmp_ht]   = FindJumps(gHat, 'conc', true);
        S_Nf_edge_est = EdgeEnhancedReconstruction(gHat, jmp_ht, jmp_locs);


        % Error (in dB)
        err_2(ix) = err_2(ix) + ...
                    10*log10( norm(f(x)-S_Nf)^2 / norm(f(x))^2 )/nTrials;
        err_edge_2(ix) = err_edge_2(ix) + ...
                    10*log10( norm(f(x)-S_Nf_edge)^2 / norm(f(x))^2 )/nTrials;
        err_edge_est_2(ix) = err_edge_est_2(ix) + ...
                    10*log10( norm(f(x)-S_Nf_edge_est)^2 / norm(f(x))^2 )/nTrials;

    end
            
end

fprintf('\n');


%% Plot error
plot( snrvals, err_2, '-+' ); hold on
plot( snrvals, err_edge_2, '-o' ); plot( snrvals, err_edge_est_2, '-x' );
plot( snrvals, ones(ix,1)*err_nonoise_SNf, '--' ); 
plot( snrvals, ones(ix,1)*err_nonoise_SNfedge, '--' );
plot( snrvals, ones(ix,1)*err_nonoise_SNfedge_est, '--' );

legend( 'Partial Fourier sum, $S_Ng_1$', ...
        'Edge Augmented Recon. (true edges), $S_N^{\rm edge}g_1$', ...
        'Edge Augmented Recon. (estimated edges), $\widetilde g_1$', ...
        'location', 'northeast' )
xlabel 'Added Noise (as SNR, in dB)'; 
ylabel 'Reconstruction Error (in dB)'; grid
axis([5 105 -75 5])
title 'Noise Robustness - Piecewise-Smooth $f$'