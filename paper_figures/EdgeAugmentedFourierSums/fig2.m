% Convergence of Fourier approximation

clear; close all; clc
addpath ../../src/


% for pretty pictures
set(0,'defaulttextinterpreter','latex');
set(0,'defaultLineLineWidth',2);
set(0,'DefaultAxesFontSize',14);
set(0,'DefaultLegendInterpreter','latex')


%% Parameters
Nvals = 2.^(5:12).';                    % Values of N
err_2           = zeros( size(Nvals) ); % Store error (2-norm) here
err_edge_2      = zeros( size(Nvals) );
err_edge_est_2  = zeros( size(Nvals) );


%% Convergence loop
for ix = 1:length(Nvals)
    N = Nvals(ix);              % No. of Fourier modes
    fprintf( '\n Now computing reconstruction error for N=%d ...', N);
    
    % Standard Fourier sum
    % Get Fourier coefficients
    [fHat, f, jmp_f] = GetFourierCoefficients( 'piecewise', N );

    % Fourier partial sum
    [S_Nf, x] = ComputeFourierReconstruction(fHat);
    % Edge-Enhanced Fourier partial sum (with true edges)
    S_Nf_edge = EdgeEnhancedReconstruction(fHat, jmp_f(:,1), jmp_f(:,2));
    
    % Edge-Enhanced Fourier partial sum (with estimated edges)
    % Jump detection - conc. method
    [jmp_locs, jmp_ht]   = FindJumps(fHat, 'conc', true);
    S_Nf_edge_est = EdgeEnhancedReconstruction(fHat, jmp_ht, jmp_locs);
    
    % Error
    h = x(2)-x(1);      % Grid size
    err_2(ix)           = sqrt(h)*norm( f(x)-S_Nf, 2 );
    err_edge_2(ix)      = sqrt(h)*norm( f(x)-S_Nf_edge, 2 );
    err_edge_est_2(ix)  = sqrt(h)*norm( f(x)-S_Nf_edge_est, 2 );
    
end

fprintf('\n');


%% Plot error
loglog( Nvals, err_2, '-+' ); hold on
loglog( Nvals, err_edge_2, '-o' ); loglog( Nvals, err_edge_est_2, '-x' );
loglog( Nvals, 9e-1*Nvals.^-0.5, '--' );
loglog( Nvals, 7e-1*Nvals.^-1.5, '--' ); grid
legend( '$\Vert f-S_Nf \Vert_2$', '$\Vert f-S_N^{\rm edge}f \Vert_2$', ...
        '$\Vert f-\widetilde f \Vert_2$', ...
        '$\mathcal O(\frac 1 {\sqrt N})$', '$\mathcal O(\frac 1 {N^{3/2}})$', ...
        'location', 'southwest' )
xlabel '$N$'; ylabel 'Reconstruction Error';
title '2-Norm Error - Piecewise-Smooth $f$'