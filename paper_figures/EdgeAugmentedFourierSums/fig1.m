% Reconstruction from Fourier spectral data
% This script demonstrates edge-augmented Fourier partial sums

clear; close all; clc
addpath ../../src

% for pretty pictures
set(0,'defaulttextinterpreter','latex');
set(0,'defaultLineLineWidth',2);
set(0,'DefaultAxesFontSize',14);
set(0,'DefaultLegendInterpreter','latex')


% Initialization and Parameters
N = 050;                        % No. of Fourier coefficients is 2N+1

% Get (analytically computed) Fourier coefficients
[fHat, f, jmp_f] = GetFourierCoefficients( 'piecewise', N );

% Fourier partial sum reconstruction
[S_Nf, x] = ComputeFourierReconstruction(fHat);

% Edge-enhanced sum (with true edges)
S_Nf_edge = EdgeEnhancedReconstruction(fHat, jmp_f(:,1), jmp_f(:,2));

% Edge-enhanced sum with estimated edges
[jmp_locs, jmp_ht]   = FindJumps(fHat, 'conc', true); 
S_Nf_edge_est = EdgeEnhancedReconstruction(fHat, jmp_ht, jmp_locs);


% Plot
plot(x, f(x), '--'); hold on; plot(x, S_Nf)
plot(x, S_Nf_edge_est, '-.'); 
grid; xlim([-pi pi])
xlabel '$x$'; ylabel 'Reconstruction'
legend( '$f_1$ (True Function)', ...
        '$S_N f_1$ (Fourier Partial Sum)', ...
        '$\widetilde f_1$ (Edge-Augmented Reconstruction)', ...
        'location', 'southwest' );
    
    
% Plot error
figure; semilogy(x, abs(f(x)-S_Nf) ); hold on
semilogy(x, abs(f(x)-S_Nf_edge_est), '-.' ); xlim([-pi pi])
xlabel '$x$'; ylabel 'Absolute Error'; grid
legend( '$|f_1-S_Nf_1|$ (Error in Partial Fourier Sum)', ...
        '$|f_1-\widetilde f_1|$ (Error in Edge-Augmented Sum)', ...
        'location', 'southwest' );