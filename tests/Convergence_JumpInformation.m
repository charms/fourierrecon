% Script to verify accuracy of edge detection code

clear; close all; clc
addpath ../src

% for pretty pictures
set(0,'defaulttextinterpreter','latex');
set(0,'defaultLineLineWidth',2);
set(0,'DefaultAxesFontSize',14);
set(0,'DefaultLegendInterpreter','latex')


%% Parameters
Nvals = 2.^(6:12).';                    % Values of N
err_prony = zeros( length(Nvals), 2 );  % Store errors here
err_conc  = zeros( length(Nvals), 2 );

addnoise = false;                       % Noisy simulation?
% addnoise = true;


%% Convergence loop
for ix = 1:length(Nvals)
    N = Nvals(ix);              % No. of Fourier modes
    
    % Get Fourier coefficients
    [fHat, f, jmp_f] = GetFourierCoefficients( 'piecewise', N );

    % Add noise?
    if(addnoise)
        snr = 30;                                       % SNR (in dB)
        sig_power = norm(fHat)^2/length(fHat);          % Signal power
        noise_power = sig_power / ( 10^(snr/10) );      % Noise power
        sig2 = noise_power;                             % Eqv. noise variance
        noise = sqrt(sig2/2)*( randn(2*N+1, 1) + 1i*randn(2*N+1, 1) );
        gHat = fHat + noise;    
    else
        gHat = fHat;
    end
    
    % Jumps using Prony method
    njumps = 06;                % Estimate for no. of jumps in function
    [jmp_loc, jmp_ht] = FindJumps(gHat, 'prony', false, [], njumps);
    
    % Compute and store error
    true_hts = zeros(njumps, 1);        % when we overestimate no. of jumps 
    true_loc = zeros(njumps, 1);
    for ijumps = 1:size(jmp_f,1)
        % Closest estimate to true jump location
        [~, minloc] = min( abs(jmp_f(ijumps,2) - jmp_loc) );
        true_hts(minloc) = jmp_f(ijumps, 1);
        true_loc(minloc) = jmp_f(ijumps, 2);
    end
    
    err_prony(ix,1) = norm( true_hts - jmp_ht, inf );
    err_prony(ix,2) = norm( true_loc - jmp_loc, inf );
%     % Error considering correct no. of jumps
%     err_prony(ix,2) = norm( true_loc(true_hts~=0) - ...
%                                             jmp_loc(true_hts~=0), inf );
    
    
    % Jumps using the concentration method
    [jmp_loc, jmp_ht]   = FindJumps(gHat, 'conc', true);

    % Compute and store error
    err_conc(ix,1) = norm( jmp_f(:,1) - jmp_ht, inf );
    err_conc(ix,2) = norm( jmp_f(:,2) - jmp_loc, inf );    
    
end


%% Plot error
loglog( Nvals, err_prony(:,1), 'r-+' ); hold on
loglog( Nvals, err_prony(:,2), 'r-o' );

loglog( Nvals, err_conc(:,1),  'b-+' );
loglog( Nvals, err_conc(:,2),  'b-o' );

loglog( Nvals, 2e-1*Nvals.^-1, '--' );
loglog( Nvals, 7e-1*Nvals.^-2, '--' ); grid
legend( 'in Jump Heights (Prony)', 'in Jump Locations (Prony)', ...
        'in Jump Heights (Conc. Method)', 'in Jump Locations (Conc. Method)', ...
        '$\mathcal O(\frac 1 N)$', '$\mathcal O(\frac 1 {N^2})$', ...
        'location', 'southwest' )
xlabel '$N$'; ylabel 'Max. Abs. Jump Estimation Error'; %axis([2e1 1e4 1e-8 2e-1])
title 'Jump Estimation Error'
