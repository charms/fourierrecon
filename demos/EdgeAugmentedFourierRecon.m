% Reconstruction from Fourier spectral data
% This script demonstrates edge-augmented Fourier partial sums

clear; %close all; clc
addpath ../src

% for pretty pictures
set(0,'defaulttextinterpreter','latex');
set(0,'defaultLineLineWidth',2);
set(0,'DefaultAxesFontSize',14);
set(0,'DefaultLegendInterpreter','latex')


% Initialization and Parameters
N = 040;                        % No. of Fourier coefficients is 2N+1

% Get (analytically computed) Fourier coefficients
[fHat, f, jmp_f] = GetFourierCoefficients( 'piecewise', N );

% Fourier partial sum reconstruction
[S_Nf, x] = ComputeFourierReconstruction(fHat);

% Edge-enhanced sum (with true edges)
S_Nf_edge = EdgeEnhancedReconstruction(fHat, jmp_f(:,1), jmp_f(:,2));

% Edge-enhanced sum with estimated edges
[jmp_locs, jmp_ht]   = FindJumps(fHat, 'conc', true); 
S_Nf_edge_est = EdgeEnhancedReconstruction(fHat, jmp_ht, jmp_locs);


% Plot
figure;
plot(x, f(x), ':'); hold on; plot(x, S_Nf)
plot(x, S_Nf_edge, '--'); plot(x, S_Nf_edge_est, '.-'); 
stem(jmp_locs, jmp_ht, 'b'); grid; xlim([-pi pi])
xlabel '$x$'; ylabel 'Reconstruction'
legend( '$f$', '$S_N f$', '$S_N^{\rm edge}f$', '$\widetilde f$', 'Edges')