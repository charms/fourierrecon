# FourierRecon: Matlab Software for Reconstruction from Fourier Spectral Data

This repository contains Matlab code for reconstructing functions from Fourier spectral data.
Specifically, it contains code for edge-augmented Fourier reconstructions as detailed in 

Edge-Augmented Fourier Partial Sums with Applications to Magnetic Resonance Imaging (MRI) [preprint][preprint]         
Jade Larriva-Latt et al.      
2016

This software was developed at the [Department of Mathematics][msumath], [Michigan State
University][msu] and is released under the MIT license. Parts of the software package 
was developed when one of the authors was at the 
[Integrated Mathematical Methods in Medical Imaging][ASUFRG] research group at 
[Arizona State University][ASU]

The software was developed and tested using Matlab R2015b. Selected scripts require the [CVX][cvx] software 
package to run successfully.


## Directory Structure and Contents

The software package is organized under the following 
directory structure:

 - demos/    
   This folder contains a representative implementation 
   of the algorithm discussed in the paper. If you have 
   just downloaded the software, execute 
   EdgeAugmentedFourierRecon.m from this folder in Matlab.

 - src/     
   This folder contains auxiliary functions necessary to 
   implement the algorithm. 

 - paper_figures/      
   This folder contains Matlab scripts for generating 
   figures from the associated paper(s).

 - tests/          
   This folder contains scripts running additional tests 
   studying the performance of the proposed methods. 


## Instructions

Extract the contents of the zip file and execute, in 
Matlab, scripts from the demos/ or paper_figures/ folders. 


## Contact

Bug reports, comments and suggestions are welcome 
at the [Bitbucket repository][bitbucket]. 


[msu]: http://www.msu.edu/
[msumath]: http://math.msu.edu/
[ASUFRG]: https://math.la.asu.edu/~asufrg/
[ASU]: https://math.asu.edu/
[bitbucket]: https://bitbucket.org/charms/fourierrecon/
[preprint]: https://math.msu.edu/~markiwen/Research.html
[cvx]: http://cvxr.com/cvx/
